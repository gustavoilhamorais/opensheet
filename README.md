# opensheet

A free API for getting Google Sheets as JSON.

**Documentation:** [benborgers.com/posts/google-sheets-json](https://benborgers.com/posts/google-sheets-json)

**If you have questions:** [benborgers.com/contact](https://benborgers.com/contact)

## Local development

```sh
vercel dev
```
